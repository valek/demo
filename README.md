# README #

Программа склеивает два файла и заменяет пробелы на подчеркивание

### Использование ###


```
#!go

./test -h
Usage of ./test:
  -d string
        Папка с файлами для слияния (default "~/")
  -f1 string
        Исходный файл (default "outfile1.txt")
  -f2 string
        Переведенный файл (default "outfile2.txt")
  -s string
        Готовый файл (default "outfile1+2.txt")
```

```
#!go

./test -d out/ -f1 out/outfile1.txt -f2 out/outfile2.txt -s in/outfile1+2.txt
```