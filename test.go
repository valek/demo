package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	filesrc := flag.String("f1", "outfile1.txt", "Исходный файл")
	filedst := flag.String("f2", "outfile2.txt", "Перведенный файл")
	pwd := flag.String("d", "~/", "Папка с файлами для слияния")

	fileNew := flag.String("s", "outfile1+2.txt", "Готовый файл")
	flag.Parse()
	input, err := ioutil.ReadFile(*filesrc)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	input2, err := ioutil.ReadFile(*filedst)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	AppendFile := string(input) + string(input2)
	filelist := make(map[int]string)
	files, err := ioutil.ReadDir(*pwd)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for j, f := range files {
		filelist[j] = f.Name()
		output := bytes.Replace([]byte(AppendFile), []byte(" "), []byte("_"), -1)
		if err = ioutil.WriteFile(*fileNew, output, 0666); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
